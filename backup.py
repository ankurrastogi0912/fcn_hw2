#!/usr/bin/env python

import dpkt

# Opening the pcap file
f = open('./assignment2.pcap')
pcap = dpkt.pcap.Reader(f)

count = 0

eth_header_len = 14

for ts, buf in pcap:
	# if len(buf) > 100:
	eth = dpkt.ethernet.Ethernet(buf)
	print eth.type, eth.__hdr_len__
	ip = eth.data
	print ip.__hdr_len__
	tcp = ip.data
	print tcp.seq
	print tcp.ack
	count = count + 1
		# if(count > 2):
		# 	break

# Clsoing the library 
f.close()
