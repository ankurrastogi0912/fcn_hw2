#!/usr/bin/env python

import dpkt
import struct





eth_header_len = 14
# count = 0
connection_flow_list = []

def parseFile(file_name):
	count = 1
	global eth_header_len
	# Opening the pcap file
	f = open(file_name,"rb")
	pcap = dpkt.pcap.Reader(f)

	global connection_flow_list

	for ts, buf in pcap:

		# print(struct.unpack(">s"))

		# print(buf[12], buf[13])
		payload_size = len(buf)
		if(buf[12] == 8 and buf[13] == 0):
			ip = buf[eth_header_len:]
			# print(ip[9])
			if(ip[9] == 6):
				ip_header_len = (ip[0] & 15) * 4
				srcIp = str(ip[12]) + "." + str(ip[13]) + "." + str(ip[14]) + "." + str(ip[15])
				dstIp = str(ip[16]) + "." + str(ip[17]) + "." + str(ip[18]) + "." + str(ip[19])
				tcp = ip[ip_header_len:]
				sport = tcp[0]<<8|tcp[1]
				dport = tcp[2]<<8|tcp[3]
				a = tcp[4]<<8|tcp[5]
				b = tcp[6]<<8|tcp[7]
				seq_no = a<<16|b
				ack_no = tcp[8]<<24|tcp[9]<<16|tcp[10]<<8|tcp[11]
				flags = (tcp[12]&1)<<8|tcp[13]
				window_size = tcp[14]<<8 | tcp[15]
				timestamp = ts
				tcp_data_offset = tcp[12]
				tcp_header_len = ((tcp_data_offset >> 4) & 15)*4
				http_data = tcp[tcp_header_len:]

				

				if flags == 18:
					continue
					# print("syn/ack --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
				if flags & 1 == 1:
					continue
					# print("fin --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
				# if flags == 16 and seq_no == 1 and ack_no == 1:
				# 	# print("ack --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
				elif flags == 2:
					# print("syn --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
					packet = {
						"id" : count,
						"srcip" : srcIp,
						"dstip" : dstIp,
						"sport": sport,
						"dport" : dport,
						"status" : 1,
						"msgs" : [],
					}
					msg = {
						"srcip" : srcIp,
						"dstip" : dstIp,
						"sport" : sport,
						"dport" : dport,
						"seq" : seq_no,
						"ack" : ack_no,
						"flags" : flags,
						"window_size" : window_size,
						"timestamp": timestamp,
						"payload_size" : payload_size,
						"http_data" : http_data
					}
					packet["msgs"].append(msg)
					connection_flow_list.append(packet)
					count = count + 1
					# print("syn --> ",sport, dport, seq_no, ack_no, flags, window_size)
				else:
					for connection in connection_flow_list:
						if ((connection["sport"] == sport and connection["dport"] == dport) or (connection["dport"] == sport and connection["sport"] == dport)):
							msg = {
								"srcip" : srcIp,
								"dstip" : dstIp,
								"sport" : sport,
								"dport" : dport,
								"seq" : seq_no,
								"ack" : ack_no,
								"flags" : flags,
								"window_size" : window_size,
								"timestamp" : timestamp,
								"payload_size" : payload_size,
								"http_data": http_data
							}
							connection["msgs"].append(msg)
				
	f.close()

def parsePlainHttpFile():
	parseFile("http_1080.pcap")

	count = 0
	first_packet_ts = connection_flow_list[0]["msgs"][0]["timestamp"]
	last_packet_ts = connection_flow_list[len(connection_flow_list)-1]["msgs"][len(connection_flow_list[len(connection_flow_list)-1]["msgs"])-1]["timestamp"]
	

	total_packet_sent = 0
	total_packet_payload_size = 0

	http_req_dict = {}

	for connection in connection_flow_list:
		for msg in connection["msgs"]:
			# if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			total_packet_sent += 1
			total_packet_payload_size += msg["payload_size"]

			if(len(msg["http_data"]) > 0):
				if(chr(msg["http_data"][0]) == 'G' and chr(msg["http_data"][1]) == 'E' and chr(msg["http_data"][2]) == 'T'):
					http_req_dict[msg["seq"]+len(msg["http_data"])] = {
						"request" : {
							"source" : str(msg["srcip"]) + ":" + str(msg["sport"]),
							"destination" : str(msg["dstip"]) + ":" + str(msg["dport"]),
							"seq" : msg["seq"],
							"ack" : msg["ack"]
						}
					}
					print("Reqest Type: GET ", str(msg["srcip"]) + ":" + str(msg["sport"]), " ---> " , str(msg["dstip"]) + ":" + str(msg["dport"]), msg["seq"], msg["ack"])


	for connection in connection_flow_list:
		for msg in connection["msgs"]:
			if(msg["sport"] == connection["dport"] and msg["dport"] == connection["sport"] and msg["ack"] in http_req_dict):
				if "response" not in http_req_dict[msg["ack"]]:
					http_req_dict[msg["ack"]]["response"] = []

				resp_obj = {
					"source" : str(msg["srcip"]) + ":" + str(msg["sport"]),
					"destination" : str(msg["dstip"]) + ":" + str(msg["dport"]),
					"seq" : msg["seq"],
					"ack" : msg["ack"]
				}

				http_req_dict[msg["ack"]]["response"].append(resp_obj)

	for request_seq in http_req_dict:
		req = http_req_dict[request_seq]["request"]
		print("")
		print("")
		print("Type --> GET Request:", req["source"], req["destination"], req["seq"], req["ack"])
		for resp in http_req_dict[request_seq]["response"]:
			print("Type --> GET Response:", resp["source"], resp["destination"], resp["seq"], resp["ack"])
		print("")
				# print("Response Type: GET ", str(msg["srcip"]) + ":" + str(msg["sport"]), " ---> " , str(msg["dstip"]) + ":" + str(msg["dport"]), msg["seq"], msg["ack"])


	print("File Name: http_1080.pcap")
	print("Total connections establised: ",len(connection_flow_list))
	print("Total time to fetch: ",last_packet_ts - first_packet_ts)
	print("The total packet sent are: ", total_packet_sent)
	print("The total packet payload size is: ", (total_packet_payload_size/(1024*1024)))
	print("")

def parsePlainHttp11File():
	parseFile("tcp_1081.pcap")

	first_packet_ts = connection_flow_list[0]["msgs"][0]["timestamp"]
	last_packet_ts = connection_flow_list[len(connection_flow_list)-1]["msgs"][len(connection_flow_list[len(connection_flow_list)-1]["msgs"])-1]["timestamp"]
	
	total_packet_sent = 0
	total_packet_payload_size = 0

	for connection in connection_flow_list:
		for msg in connection["msgs"]:
			# if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			total_packet_sent += 1
			total_packet_payload_size += msg["payload_size"]

	print("File Name: tcp_1081.pcap")
	print("Total connections establised: ",len(connection_flow_list))
	print("Total time to fetch: ",last_packet_ts - first_packet_ts)
	print("The total packet sent are: ", total_packet_sent)
	print("The total packet payload size is: ", (total_packet_payload_size/(1024*1024)))
	print("")

def parsePlainHttp20File():
	parseFile("tcp_1082.pcap")

	first_packet_ts = connection_flow_list[0]["msgs"][0]["timestamp"]
	last_packet_ts = connection_flow_list[len(connection_flow_list)-1]["msgs"][len(connection_flow_list[len(connection_flow_list)-1]["msgs"])-1]["timestamp"]
	print(last_packet_ts - first_packet_ts)
	
	total_packet_sent = 0
	total_packet_payload_size = 0

	for connection in connection_flow_list:
		for msg in connection["msgs"]:
			# if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			total_packet_sent += 1
			total_packet_payload_size += msg["payload_size"]

	print("File Name: tcp_1082.pcap")
	print("Total connections establised: ",len(connection_flow_list))
	print("Total time to fetch: ",last_packet_ts - first_packet_ts)
	print("The total packet sent are: ", total_packet_sent)
	print("The total packet payload size is: ", (total_packet_payload_size/(1024*1024)))
	print("")


def init():
	global connection_flow_list
	connection_flow_list = []
	parsePlainHttpFile()
	connection_flow_list = []
	parsePlainHttp11File()
	connection_flow_list = []
	parsePlainHttp20File()
	
init()