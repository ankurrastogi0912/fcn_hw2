#!/usr/bin/env python

import dpkt

# Opening the pcap file
f = open("assignment2.pcap","rb")
pcap = dpkt.pcap.Reader(f)

count = 1

eth_header_len = 14
# count = 0

connection_flow_list = []

for ts, buf in pcap:
	# print(buf[12], buf[13])
	payload_size = len(buf)
	if(buf[12] == 8 and buf[13] == 0):
		ip = buf[eth_header_len:]
		# print(ip[9])
		if(ip[9] == 6):
			ip_header_len = (ip[0] & 15) * 4
			srcIp = str(ip[12]) + "." + str(ip[13]) + "." + str(ip[14]) + "." + str(ip[15])
			dstIp = str(ip[16]) + "." + str(ip[17]) + "." + str(ip[18]) + "." + str(ip[19])
			tcp = ip[ip_header_len:]
			sport = tcp[0]<<8|tcp[1]
			dport = tcp[2]<<8|tcp[3]
			a = tcp[4]<<8|tcp[5]
			b = tcp[6]<<8|tcp[7]
			seq_no = a<<16|b
			ack_no = tcp[8]<<24|tcp[9]<<16|tcp[10]<<8|tcp[11]
			flags = (tcp[12]&1)<<8|tcp[13]
			window_size = tcp[14]<<8 | tcp[15]
			timestamp = ts
			tcp_data_offset = tcp[12]
			tcp_header_len = ((tcp_data_offset >> 4) & 15)*4
			http_data = tcp[tcp_header_len:]
			# if flags == 18:
			# 	# print("syn/ack --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
			# if flags & 1 == 1:
				# print("fin --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
			# if flags == 16 and seq_no == 1 and ack_no == 1:
			# 	# print("ack --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
			if flags == 2:
				# print("syn --> ",sport, dport, seq_no, ack_no, flags, window_size, timestamp)
				packet = {
					"id" : count,
					"srcip" : srcIp,
					"dstip" : dstIp,
					"sport": sport,
					"dport" : dport,
					"status" : 1,
					"msgs" : [],
				}
				msg = {
					"srcip" : srcIp,
					"dstip" : dstIp,
					"sport" : sport,
					"dport" : dport,
					"seq" : seq_no,
					"ack" : ack_no,
					"flags" : flags,
					"window_size" : window_size,
					"timestamp": timestamp,
					"payload_size" : payload_size,
					"http_data": http_data
				}
				packet["msgs"].append(msg)
				connection_flow_list.append(packet)
				count = count + 1
				# print("syn --> ",sport, dport, seq_no, ack_no, flags, window_size)
			else:
				for connection in connection_flow_list:
					if ((connection["sport"] == sport and connection["dport"] == dport) or (connection["dport"] == sport and connection["sport"] == dport)):
						msg = {
							"srcip" : srcIp,
							"dstip" : dstIp,
							"sport" : sport,
							"dport" : dport,
							"seq" : seq_no,
							"ack" : ack_no,
							"flags" : flags,
							"window_size" : window_size,
							"timestamp" : timestamp,
							"payload_size" : payload_size,
							"http_data" : http_data
						}
						connection["msgs"].append(msg)
			
			# print("ack --> ",sport, dport, seq_no, ack_no, flags, window_size, tcp[5:9])

			# break

def noOfConnection():
	print("Part A 1: TCP flows initiated from the sender = ",len(connection_flow_list))

def findThrougput(connection):
	sendNo = 0
	sendPayloadSize = 0
	startTimestamp = connection["msgs"][0]["timestamp"]
	endTimestamp = connection["msgs"][len(connection["msgs"])-1]["timestamp"]

	for msg in connection["msgs"]:
		if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			sendNo = sendNo + 1
			sendPayloadSize = sendPayloadSize + msg["payload_size"]

	timeDiff = endTimestamp - startTimestamp
	print("Throughput: ", sendPayloadSize/(timeDiff*1024*1024))

def findAvgRtt(connection):
	msg_dict = {}
	for msg in connection["msgs"]:
		if msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]:
			msg_dict[msg["seq"]+len(msg["http_data"])] = {
				"send_time" : msg["timestamp"],
				"recieved_time" : 0
			}

	for msg in connection["msgs"]:
		if msg["sport"] == connection["dport"] and msg["dport"] == connection["sport"]:
			if(msg["ack"] in msg_dict):
				if msg_dict[msg["ack"]]["recieved_time"] == 0:
					msg_dict[msg["ack"]]["recieved_time"] = msg["timestamp"]

	total_packets = 0
	totalRttTime = 0
	for item in msg_dict:
		if msg_dict[item]["recieved_time"] != 0:
			totalRttTime += (msg_dict[item]["recieved_time"] - msg_dict[item]["send_time"])
			total_packets += 1

	print("Average RTT: ", (totalRttTime/total_packets))

def returnFirstTwoTransactions(connection):
	req_obj = {}
	resp_obj = {}
	for msg in connection["msgs"]:
		if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			req_obj[msg["seq"]+len(msg["http_data"])] = {
				"seq" : msg["seq"],
				"ack" : msg["ack"],
				"window_size" : msg["window_size"]
			}
		if(msg["sport"] == connection["dport"] and msg["dport"] == connection["sport"]):
			if msg["ack"] not in resp_obj:
				resp_obj[msg["ack"]] = {
					"seq" : msg["seq"],
					"ack" : msg["ack"],
					"window_size" : msg["window_size"]
				}

	transaction_count = 1
	for req in req_obj:
		if req in resp_obj:
			print("Transaction " + str(transaction_count) + ": ")
			print("Seq: ", req_obj[req]["seq"], ", Ack: ", req_obj[req]["ack"], ", Window Size: ", req_obj[req]["window_size"])
			print("Seq: ", resp_obj[req]["seq"], ", Ack: ", resp_obj[req]["ack"], ", Window Size: ", resp_obj[req]["window_size"])
			transaction_count += 1
			if(transaction_count >= 3):
				break
	print("")

def findLossRate(connection):
	send_dict = {}
	total_packet_loss = 0
	total_msg_sent = 0
	for msg in connection["msgs"]:
		if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			total_msg_sent += 1
			if(msg["seq"] in send_dict):
				send_dict[msg["seq"]] += 1
			else :
				send_dict[msg["seq"]] = 1

	for msg in send_dict:
		if send_dict[msg] > 1:
			total_packet_loss += send_dict[msg]

	print("Loss Rate: ", total_packet_loss/total_msg_sent)

def calculateCongestionWindow(connection):
	seq_no = 0
	window_cal_timer = 0
	for msg in connection["msgs"]:
		if msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]:
			seq_no = msg["seq"] + len(msg["http_data"])
		if(msg["sport"] == connection["dport"] and msg["dport"] == connection["sport"] and seq_no != 0 and seq_no - msg["ack"] > 0):
			window_cal_timer = window_cal_timer + 1
			print("Current window size is: " + str(seq_no - msg["ack"]))
			seq_no = 0

		if(window_cal_timer >= 10):
			break

def calculateRetransMission(connection):
	total_retransmission = 0
	triple_ack_retransmission = 0
	total_sent = 0

	send_dict = {}
	recieve_dict = {}

	for msg in connection["msgs"]:
		if(msg["sport"] == connection["sport"] and msg["dport"] == connection["dport"]):
			if(msg["seq"] not in send_dict):
				send_dict[msg["seq"]] = 1
			else :
				send_dict[msg["seq"]] += 1
		if(msg["sport"] == connection["dport"] and msg["dport"] == connection["sport"]):
			if(msg["ack"] not in recieve_dict):
				recieve_dict[msg["ack"]] = 1
			else :
				recieve_dict[msg["ack"]] += 1

	for item in send_dict:
		if(send_dict[item] > 1):
			total_retransmission += send_dict[item]

	for msg in connection["msgs"]:
		if msg["seq"] in recieve_dict and recieve_dict[msg["seq"]] > 2:
			triple_ack_retransmission += 1

	# print("total_retransmission: ",total_retransmission)
	print("triple_ack_retransmission: ",triple_ack_retransmission)
	print("timeout_retransmission: ",total_retransmission - triple_ack_retransmission)


def init():
	noOfConnection()

	# Part A 2(i)
	print("Part A 2(i)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		returnFirstTwoTransactions(connection)
		count += 1

	print("Part A 2(ii)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		findThrougput(connection)
		count += 1
	print("")

	print("Part A 2(iii)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		findLossRate(connection)
		count += 1
	print("")

	print("Part A 2(iv)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		findAvgRtt(connection)
		count += 1
	print("")

	print("Part B (1)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		calculateCongestionWindow(connection)
		count += 1
	print("")

	print("Part B (2)")
	count = 1
	for connection in connection_flow_list:
		print("Flow"+str(count)+": ")
		calculateRetransMission(connection)
		count += 1
	print("")

init()

# print(count)

# Clsoing the library 
f.close()
